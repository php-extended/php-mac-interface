<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-mac-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Mac;

/**
 * MacAddress64BitsInterface interface file.
 * 
 * This interface represents a mac address on a 64-byte hexadecimal string.
 * 
 * @author Anastaszor
 * @see https://tools.ietf.org/html/rfc5342
 */
interface MacAddress64BitsInterface extends MacAddress48BitsInterface
{
	
	/**
	 * Gets the two additional bytes of the Organisationally Unique Identifier,
	 * on 16 bits. It represents the two midle bytes of the mac address.
	 * 
	 * @return integer
	 */
	public function getLnk() : int;
	
	/**
	 * Gets the seventh byte of this mac address.
	 * 
	 * @return integer
	 */
	public function getSeventhByte() : int;
	
	/**
	 * Gets the eighth byte of this mac address.
	 * 
	 * @return integer
	 */
	public function getEighthByte() : int;
	
}
