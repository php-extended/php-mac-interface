<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-mac-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Mac;

use Stringable;

/**
 * MacAddress48BitsInterface interface file.
 * 
 * This interface represents a mac address on a 48-byte hexadecimal string.
 * 
 * @author Anastaszor
 * @see https://tools.ietf.org/html/rfc5342
 */
interface MacAddress48BitsInterface extends Stringable
{
	
	/**
	 * Gets the Organisationally Unique Identifier on 24 bits. It represents the
	 * first three bytes of the mac address.
	 * 
	 * @return integer
	 */
	public function getOui() : int;
	
	/**
	 * Gets the Network Interface Controller on 24 bits. It represents the
	 * last three bytes of the mac address.
	 * 
	 * @return integer
	 */
	public function getNic() : int;
	
	/**
	 * Gets the first byte of this mac address.
	 * 
	 * @return integer
	 */
	public function getFirstByte() : int;
	
	/**
	 * Gets the second byte of this mac address.
	 * 
	 * @return integer
	 */
	public function getSecondByte() : int;
	
	/**
	 * Gets the third byte of this mac address.
	 * 
	 * @return integer
	 */
	public function getThirdByte() : int;
	
	/**
	 * Gets the fourth byte of this mac address.
	 * 
	 * @return integer
	 */
	public function getFourthByte() : int;
	
	/**
	 * Gets the fifth byte of this mac address.
	 * 
	 * @return integer
	 */
	public function getFifthByte() : int;
	
	/**
	 * Gets the sixth byte of this mac address.
	 * 
	 * @return integer
	 */
	public function getSixthByte() : int;
	
	/**
	 * Gets whether this mac address is for unicast use (i.e. for an unique
	 * machine).
	 * 
	 * @return boolean
	 */
	public function isUnicast() : bool;
	
	/**
	 * Gets whether this mac address is for unicast use (i.e. for multiple
	 * machine).
	 * 
	 * @return boolean
	 */
	public function isMulticast() : bool;
	
	/**
	 * Gets whether this mac address is globally unique (i.e. conforms to the
	 * IEEE format).
	 * 
	 * @return boolean
	 */
	public function isGloballyUnique() : bool;
	
	/**
	 * Gets whether this mac address is locally unique (i.e. does not conform
	 * to the IEEE format).
	 * 
	 * @return boolean
	 */
	public function isLocallyUnique() : bool;
	
	/**
	 * Gets whether this mac address is an ipv4 multicast address.
	 * 
	 * @return boolean
	 * @see https://tools.ietf.org/html/rfc1112
	 */
	public function isIpv4Multicast() : bool;
	
	/**
	 * Gets whether this mac address is equal to another mac address.
	 * 
	 * @param null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>> $object
	 * @return boolean
	 */
	public function equals($object) : bool;
	
}
