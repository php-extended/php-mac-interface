<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-mac-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Mac;

use PhpExtended\Parser\ParserInterface;

/**
 * MacAddress48ParserInterface interface file.
 * 
 * This class represents a parser for 48 bits mac addresses.
 * 
 * @author Anastaszor
 * @extends \PhpExtended\Parser\ParserInterface<MacAddress48BitsInterface>
 */
interface MacAddress48ParserInterface extends ParserInterface
{
	
	// nothing to add
	// php does not accepts covariant return types between interfaces
	
}
